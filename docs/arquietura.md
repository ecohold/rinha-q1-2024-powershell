0) inicialize e configure mvcc, multitenancy, in-memory
0.1) implemente um arquivo por vez sequencialmente e para eu testar
1) padrao senior devop gcp 
2) completo, produção, validado, testado, certificado, auditado
3) refatore a estrutura atual para as novas demandas com todos novos arquivos


        rinha-q1-2024-powershell/
        ├── data
        │ ├── database.db
        │ ├── database.db-shm
        │ └── database.db-wal
        ├── database.psm1
        ├── docker-compose.yml
        ├── Dockerfile
        ├── executar-teste-local.sh
        ├── images
        │ └── 45456617.webp
        ├── load-test
        │ └── user-files
        │ └── simulations
        │ └── rinhabackend
        │ └── RinhaBackendCrebitosSimulation.scala
        ├── nginx.conf
        ├── README.md
        ├── resultados
        ├── run.sh
        ├── scripts
        │ └── init.sql
        └── server.ps1

#------------init.sql------------------#

CREATE TABLE IF NOT EXISTS clientes (
 id INTEGER PRIMARY KEY,
 nome TEXT NOT NULL,
 limite INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS transacoes (
 id INTEGER PRIMARY KEY,
 cliente_id INTEGER NOT NULL,
 valor INTEGER NOT NULL,
 tipo TEXT NOT NULL,
 descricao TEXT NOT NULL,
 realizada_em TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

CREATE TABLE IF NOT EXISTS saldos (
 id INTEGER PRIMARY KEY,
 cliente_id INTEGER NOT NULL,
 valor INTEGER NOT NULL,
 FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

CREATE INDEX id_index ON clientes (id);
CREATE INDEX id_cliente_transacoes_index ON transacoes (cliente_id);
CREATE INDEX id_cliente_saldos_index ON saldos (cliente_id);

INSERT INTO clientes (nome, limite)
	VALUES
		('ze da manga', 1000 * 100),
		('vai neymar', 800 * 100),
		('loteria', 10000 * 100),
		('nossa', 100000 * 100),
		('o que? como?', 5000 * 100);
	
INSERT INTO saldos (cliente_id, valor)
 SELECT id, 0 FROM clientes;

6) restrição de projeto: api em bash, banco sqlite in-memory, multitency, mvcc

#-------------server--server.ps1----------------#

Import-Module "/usr/local/share/powershell/Modules/SQLite/PSSQLite.psm1"
Import-Module "/database.psm1"

# Algumas variáveis para ajudar a configurar o servidor
$port = $env:PORT

$databasePath = "/app/data/database.db"
$scriptPath = "/app/scripts/init.sql"
$tabelas = @("clientes", "transacoes", "saldos")
$tabelasValidas = 0


class Database {
    [string] $databasePath = "/app/data/database.db"
    [int] $maxPoolSize = 200
    [System.Collections.Generic.List[object]] $connectionPool

    Database() {
        $this.connectionPool = [System.Collections.Generic.List[object]]::new()

        # Inicializa o pool de conexão
        for ($i = 0; $i -lt $this.maxPoolSize; $i++) {
            $connection = New-SQLiteConnection -DataSource $this.databasePath
            $this.connectionPool.Add($connection)
        }
        $this.SetJournalModeWAL()
        $this.SetSynchronousNormal()
        $this.SetBusyTimeout(15000)
    }
    [void] SetJournalModeWAL() {
        $query = "PRAGMA journal_mode = WAL;"
        foreach ($conn in $this.connectionPool) {
            $command = $conn.CreateCommand()
            $command.CommandText = $query
            $command.ExecuteNonQuery()
        }
    }
    [void] SetBusyTimeout($timeout) {
        $query = "PRAGMA busy_timeout = $timeout;"
        foreach ($conn in $this.connectionPool) {
            $command = $conn.CreateCommand()
            $command.CommandText = $query
            $command.ExecuteNonQuery()
        }
    }
    [void] SetSynchronousNormal() {
        $query = "PRAGMA synchronous = NORMAL;"
        foreach ($conn in $this.connectionPool) {
            $command = $conn.CreateCommand()
            $command.CommandText = $query
            $command.ExecuteNonQuery()
        }
    }
    [object] GetConnectionFromPool() {
        foreach ($conn in $this.connectionPool) {
            if ($conn.State -eq 'Open') {
                return $conn
            }
        }

        # Se não houver conexões disponíveis no pool, cria uma nova
        $connection = New-SQLiteConnection -DataSource $this.databasePath
        $this.connectionPool.Add($connection)
        return $connection
    }

    [bool] isClient($id) {
        $sqlQuery = "SELECT id FROM clientes WHERE id = $id;"
        $selectedRecord = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
        if($selectedRecord){
            return $true
        }
        return $false
    }

    [object] getClient($id) {
        $sqlQuery = "SELECT nome, limite FROM clientes WHERE id = ${id};"
        $cliente = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
        return $cliente
    }

    [object] getSaldo($id) {
        $sqlQuery = "SELECT valor FROM saldos WHERE cliente_id = ${id};"
        $saldo = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
        return $saldo
    }

    [void] updateSaldo($id, $saldo) {
        $sqlQuery = "UPDATE saldos SET valor = $saldo WHERE cliente_id = $id;"
        Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
    }

    [void] saveTransacao($id, $valor, $tipo, $descricao) {
        $sqlQuery = "INSERT INTO transacoes (cliente_id, valor, tipo, descricao, realizada_em) VALUES ($id, $valor, '$tipo', '$descricao', datetime('now'));"
        Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
    }

    [object] getTransacoes($id) {
        $sqlQuery = "SELECT valor, tipo, descricao, realizada_em FROM transacoes WHERE cliente_id = $id ORDER BY realizada_em DESC LIMIT 10;"
        $transacoes = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
        return $transacoes
    }

    [object] getExtrato($id) {
        $sqlQuery = "SELECT c.limite as limite, datetime('now', 'localtime') as data_extrato, s.valor as total 
        FROM clientes c 
        JOIN saldos s on c.id = s.cliente_id 
        WHERE c.id = $id;"
        $transacoes = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool()
        return $transacoes
    }

    [void] BeginTransaction() {
        $connection = $this.GetConnectionFromPool()
        Invoke-SqliteQuery -Query "BEGIN EXCLUSIVE;" -SQLiteConnection $connection
    }

    [void] CommitTransaction() {
        $connection = $this.GetConnectionFromPool()
        Invoke-SqliteQuery -Query "COMMIT;" -SQLiteConnection $connection
    }

    [void] RollbackTransaction() {
        $connection = $this.GetConnectionFromPool()
        Invoke-SqliteQuery -Query "ROLLBACK;" -SQLiteConnection $connection
    }

    # Fecha todas as conexões no pool
    [void] CloseConnection() {
        foreach ($conn in $this.connectionPool) {
            $conn.Close()
        }
    }
}

# Verificar se o banco de dados existe
if($env:MIGRATE){
    Check-Database -databasePath $databasePath -scriptPath $scriptPath -tabelas $tabelas -tabelasValidas $tabelasValidas
}

# Função para Processar as transações
function Processar-Transacao {
    param(
        [System.Net.HttpListenerRequest]$Request,
        [System.Net.HttpListenerResponse]$Response,
        [Database]$database
    )
    $url = $Request.RawUrl.Split("/")
    $IdDoCliente = $url[-2]
   
    if($IdDoCliente -gt 5) {
        $Response.StatusCode = 404
        $Response.StatusDescription = "Not Found"
        return $Response.Close()
    }

    $database.BeginTransaction()
    $isCliente = $database.isClient($IdDoCliente)
    Write-Host $isCliente
    if(!$isCliente){
        $database.RollbackTransaction();
        $Response.StatusCode = 404
        $Response.StatusDescription = "Not Found"
        return $Response.Close()
    }
    $body = $request.InputStream
    $streamReader = New-Object System.IO.StreamReader($body, [System.Text.Encoding]::UTF8)
    $json = $streamReader.ReadToEnd()
    $streamReader.Close()

    # Converter o JSON em objeto PowerShell
    $transacao = ConvertFrom-Json $json
    $integerNumber = 0
    Write-Host $transacao
    $isInt = [int]::TryParse($transacao.valor, [ref]$integerNumber)
    $valor = $transacao.valor
    Write-Host $isInt
    if ($isInt) {
       $valor = $integerNumber
    } else {
        $database.RollbackTransaction();
        Write-Host "Valor inválido"
        $Response.StatusCode = 422
        $Response.StatusDescription = "Unprocessable Entity"
        return $Response.Close()
    }
    if ($valor -le 0) {
        $database.RollbackTransaction();
        Write-Host "Valor inválido"
        $Response.StatusCode = 422
        $Response.StatusDescription = "Unprocessable Entity"
        return $Response.Close()
    }
    
    if($transacao.tipo -notin @('c', 'd')) {
        $database.RollbackTransaction();
        Write-Host "Tipo inválido"
        $Response.StatusCode = 422
        $Response.StatusDescription = "Unprocessable Entity"
        return $Response.Close()
    }
    
    if($transacao.descricao.Length -gt 10 -or $transacao.descricao -eq "" -or $transacao.descricao -eq $null){
        $database.RollbackTransaction();
        Write-Host "Descrição inválida"
        $Response.StatusCode = 422
        $Response.StatusDescription = "Unprocessable Entity"
        return $Response.Close()
    }
    $novoSaldo = 0;
    $cliente = $database.getClient($IdDoCliente)
    $saldo = $database.getSaldo($IdDoCliente)
    Write-Host $cliente
    Write-Host $saldo
    $novoSaldo = $saldo.valor + $valor
    Write-Host $novoSaldo
    if($transacao.tipo -eq 'd'){
        $novoSaldo = $saldo.valor - $valor
        if($novoSaldo -lt -$cliente.limite){
            $database.RollbackTransaction();
            $Response.StatusCode = 422
            $Response.StatusDescription = "Unprocessable Entity"
            return $Response.Close()
        }
    }
    $database.updateSaldo($IdDoCliente, $novoSaldo)
    $database.saveTransacao($IdDoCliente, $valor, $transacao.tipo, $transacao.descricao)
    $responseJson = @{
        limite = $cliente.limite
        saldo = $novoSaldo
    } | ConvertTo-Json
    
    # Configurar o status de resposta (200 OK)
    $Response.StatusCode = 200
    $Response.StatusDescription = "OK"
    $Response.OutputStream.Write([System.Text.Encoding]::UTF8.GetBytes($responseJson), 0, $responseJson.Length)
    $database.CommitTransaction();
    # Fechar o OutputStream
    return $Response.Close()
}
function Gerar-Extrato {
    param(
        [System.Net.HttpListenerRequest]$Request,
        [System.Net.HttpListenerResponse]$Response,
        [Database]$database
    )
    $url = $Request.RawUrl.Split("/")
    $IdDoCliente = $url[-2]
    if($IdDoCliente -gt 5) {
        $Response.StatusCode = 404
        $Response.StatusDescription = "Not Found"
        return $Response.Close()
    }
    Write-Host $url
    $database.BeginTransaction()
    $isCliente = $database.isClient($IdDoCliente)
    Write-Host $isCliente
    if(!$isCliente){
        $database.RollbackTransaction();
        $Response.StatusCode = 404
        $Response.StatusDescription = "Not Found"
        return $Response.Close()
    }

    $transacoes = $database.getTransacoes($IdDoCliente)
    $saldo = $database.getExtrato($IdDoCliente)
    Write-Host $saldo
    Write-Host $transacoes
    $database.CommitTransaction();

    $transacoesList = foreach ($row in $transacoes) {
        @{
            valor = $row.valor_transacao
            tipo = $row.tipo_transacao
            descricao = $row.descricao_transacao
            realizada_em = $row.data_transacao
        }
    }

    # Formatar os resultados como JSON
    $jsonResult = @{
        saldo = @{
            total = $saldo.total
            data_extrato = $saldo.data_extrato
            limite = $saldo.limite
        }
        ultimas_transacoes = $transacoes
    } | ConvertTo-Json
     # Definir o cabeçalho de resposta
     $response.StatusCode = 200
     $response.ContentType = "application/json"
     # Escrever os resultados na resposta
     $response.OutputStream.Write([System.Text.Encoding]::UTF8.GetBytes($jsonResult), 0, $jsonResult.Length)

    # Configurar o status de resposta (200 OK)
    $Response.StatusCode = 200
    $Response.StatusDescription = "OK"

    # Fechar o OutputStream
    return $Response.Close()
}

$patternRouteTx = "\/clientes\/(\d+)\/transacoes$"
$patternRouteExtrato = "\/clientes\/(\d+)\/extrato$"

$listener = New-Object System.Net.HttpListener
$listener.Prefixes.Add("http://*:$port/")
# Inicia o listener
$listener.Start()

Write-Host "Servidor web iniciado. Aguardando requisições..."

try {
    $database = [Database]::new()
    while ($true) {
        # Aguarda de forma assíncrona por uma requisição
        $context = $listener.GetContext()

        # Obtém a requisição e a resposta do contexto
        $request = $context.Request
        $response = $context.Response
        Write-Host "Requisição recebida: $($request.HttpMethod) $($request.Url.AbsolutePath)"
        # Verifique se a URL da requisição corresponde ao padrão
        if ($request.HttpMethod -eq "POST" -and $request.Url.AbsolutePath -match $patternRouteTx) {
            # Se corresponder, processe a transação
            Write-Host "Processando transação..."
            Processar-Transacao $request $response $database
        }
        if ($request.HttpMethod -eq "GET" -and $request.Url.AbsolutePath -match $patternRouteExtrato) {
            # Se corresponder, processe a transação
            Write-Host "Gerando Extrato..."
            Gerar-Extrato $request $response $database
        }
    }
}
finally {
    # Certifica-se de que o listener seja fechado adequadamente
    $listener.Stop()
    $listener.Close()
}

#-----------------------------run.sh-----------------------#

rm -rf ./data/database.db && docker-compose down --remove-orphans && docker-compose up --build

#---------------database.psm1-----------------#

Import-Module "/usr/local/share/powershell/Modules/SQLite/PSSQLite.psm1"

function Check-Database {
  param (
    [string]$databasePath,
    [string]$scriptPath,
    [array]$tabelas,
    [int]$tabelasValidas
  )
  if (Test-Path $databasePath) {
    Write-Output "O arquivo SQLite já existe."
    $tabelas = @("clientes", "transacoes", "saldos")

    # Loop através das tabelas
    foreach ($tabela in $tabelas) {
      # Consulta SQL para verificar a existência da tabela na tabela sqlite_master
      $query = "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='$tabela';"

      # Executar a consulta
      $resultado = Invoke-SQLiteQuery -DataSource $databasePath -Query $query

      # Acessar o valor real retornado pela consulta
      $count = $resultado.'COUNT(*)'

      # Verificar o resultado
      if ($count -gt 0) {
        Write-Output "A tabela '$tabela' existe no banco de dados."
        $tabelasValidas++
      } else {
        Write-Output "A tabela '$tabela' não existe no banco de dados."
      }
    }
    if ($tabelasValidas -eq 3) {
      Write-Output "O banco de dados está pronto para uso."
    } else {
      Write-Output "O banco de dados não está pronto para uso."
      $data = Get-Content -Path $scriptPath -Raw
      Invoke-SqliteQuery -DataSource $databasePath -Query $data
    }
  } else {
    # Criar o arquivo SQLite
    New-Item -Path $databasePath -ItemType File
    Write-Output "Arquivo SQLite criado com sucesso."
    $data = Get-Content -Path $scriptPath -Raw
    Invoke-SqliteQuery -DataSource $databasePath -Query $data
  }
}

Export-ModuleMember -Function Check-Database

#----------Dockerfile------------------#

# Use uma imagem do PowerShell como base
FROM mcr.microsoft.com/powershell

# Copie o script PowerShell para dentro do contêiner
COPY server.ps1 /server.ps1
COPY database.psm1 /database.psm1
COPY ./scripts/init.sql /app/scripts/init.sql
#RUN pwsh -Command "Install-Module -Name SQLite -Force -AllowClobber"
# Baixe o arquivo SQLite.zip usando Invoke-WebRequest
RUN pwsh -Command "Invoke-WebRequest -Uri 'https://www.powershellgallery.com/api/v2/package/PSSQLite' -OutFile 'SQLite.zip'"

RUN pwsh -Command "Expand-Archive -Path 'SQLite.zip' -DestinationPath '/usr/local/share/powershell/Modules/SQLite'"


# Exponha a porta 8080 para o servidor
EXPOSE 8080
EXPOSE 8081

# Execute o script PowerShell quando o contêiner for iniciado
CMD ["pwsh", "/server.ps1"]


#-----------------docker-compose.yml------#

        version: "3.5"


        services:
        api01: &api
            hostname: api01
            volumes:
            - ./data:/app/data
            networks:
            - default
            build:
            context: .
            dockerfile: ./Dockerfile
            environment:
            - MIGRATE=1
            - PORT=8080
            ports:
            - "8080:8080"
            deploy:
            resources:
                limits:
                cpus: "0.65"
                memory: "250MB"
        api02:
            <<: *api
            hostname: api02
            environment:
            - PORT=8081
            ports:
            - "8081:8081"
        nginx:
            image: nginx:latest
            volumes:
            - ./nginx.conf:/etc/nginx/nginx.conf:ro
            depends_on:
            - api01
            - api02
            ports:
            - "9999:9999"
            deploy:
            resources:
                limits:
                cpus: "0.2"
                memory: "50MB"

        networks:
        default:
            driver: bridge
            name: rinha-network
