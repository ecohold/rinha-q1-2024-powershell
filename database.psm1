#-----------database.psm1-------------#

# Importa o módulo SQLite
Import-Module "/usr/local/share/powershell/Modules/SQLite/PSSQLite.psm1"

# Função para verificar e configurar o banco de dados
function Check-Database {
  param (
    [string]$databasePath,
    [string]$scriptPath,
    [array]$tables,
    [int]$validTablesCount
  )

  if (Test-Path $databasePath) {
    Write-Output "O banco de dados SQLite já existe."

    # Loop através das tabelas
    foreach ($table in $tables) {
      # Consulta SQL para verificar a existência da tabela na tabela sqlite_master
      $query = "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='$table';"

      # Executar a consulta
      $result = Invoke-SQLiteQuery -DataSource $databasePath -Query $query

      # Acessar o valor real retornado pela consulta
      $count = $result.'COUNT(*)'

      # Verificar o resultado
      if ($count -gt 0) {
        Write-Output "A tabela '$table' existe no banco de dados."
        $validTablesCount++
      } else {
        Write-Output "A tabela '$table' não existe no banco de dados."
      }
    }

    if ($validTablesCount -eq $tables.Count) {
      Write-Output "O banco de dados está pronto para uso."
    } else {
      Write-Output "O banco de dados não está pronto para uso."
      $data = Get-Content -Path $scriptPath -Raw
      Invoke-SQLiteQuery -DataSource $databasePath -Query $data
    }
  } else {
    # Criar o arquivo SQLite
    New-Item -Path $databasePath -ItemType File
    Write-Output "Arquivo SQLite criado com sucesso."
    $data = Get-Content -Path $scriptPath -Raw
    Invoke-SQLiteQuery -DataSource $databasePath -Query $data
  }
}

# Exporta a função para que ela esteja disponível quando o módulo for importado
Export-ModuleMember -Function Check-Database
