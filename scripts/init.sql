-- Adicionando identificador de locação aos dados de cliente e saldo
CREATE TABLE IF NOT EXISTS clientes (
    id INTEGER PRIMARY KEY,
    nome TEXT NOT NULL,
    limite INTEGER NOT NULL,
    location_id INTEGER NOT NULL  -- Identificador de locação
);

CREATE TABLE IF NOT EXISTS transacoes (
    id INTEGER PRIMARY KEY,
    cliente_id INTEGER NOT NULL,
    valor INTEGER NOT NULL,
    tipo TEXT NOT NULL,
    descricao TEXT NOT NULL,
    realizada_em TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    location_id INTEGER NOT NULL,  -- Identificador de locação
    FOREIGN KEY (cliente_id, location_id) REFERENCES clientes(id, location_id)  -- Chave estrangeira com identificador de locação
);

CREATE TABLE IF NOT EXISTS saldos (
    id INTEGER PRIMARY KEY,
    cliente_id INTEGER NOT NULL,
    valor INTEGER NOT NULL,
    location_id INTEGER NOT NULL,  -- Identificador de locação
    FOREIGN KEY (cliente_id, location_id) REFERENCES clientes(id, location_id)  -- Chave estrangeira com identificador de locação
);

-- Índices
CREATE INDEX id_index ON clientes (id, location_id);
CREATE INDEX id_cliente_transacoes_index ON transacoes (cliente_id, location_id);
CREATE INDEX id_cliente_saldos_index ON saldos (cliente_id, location_id);

-- Inserção de dados de teste com identificadores de locação
INSERT INTO clientes (nome, limite, location_id)
	VALUES
		('ze da manga', 1000 * 100, 1),
		('vai neymar', 800 * 100, 2),
		('loteria', 10000 * 100, 3),
		('nossa', 100000 * 100, 4),
		('o que? como?', 5000 * 100, 5);
	
INSERT INTO saldos (cliente_id, valor, location_id)
    SELECT id, 0, location_id FROM clientes;
