Import-Module "/usr/local/share/powershell/Modules/SQLite/PSSQLite.psm1"

# Algumas variáveis para ajudar a configurar o servidor
$port = $env:PORT

$databasePath = ":memory:"  # Alterado para memória
$scriptPath = "/app/scripts/init.sql"
$tabelas = @("clientes", "transacoes", "saldos")
$tabelasValidas = 0


class Database {
    [string] $databasePath = ":memory:"  # Alterado para memória
    [int] $maxPoolSize = 200
    [System.Collections.Generic.Dictionary[string, System.Collections.Generic.List[object]]] $connectionPool  # Alterado para suportar multitenancy

    Database() {
        $this.connectionPool = [System.Collections.Generic.Dictionary[string, System.Collections.Generic.List[object]]]::new()

        # Inicializa o pool de conexão
        foreach ($tabela in $tabelas) {
            $this.connectionPool[$tabela] = [System.Collections.Generic.List[object]]::new()
            for ($i = 0; $i -lt $this.maxPoolSize; $i++) {
                $connection = New-SQLiteConnection -DataSource $this.databasePath
                $this.connectionPool[$tabela].Add($connection)
            }
        }
        $this.SetJournalModeWAL()
        $this.SetSynchronousNormal()
        $this.SetBusyTimeout(15000)
    }
    [void] SetJournalModeWAL() {
        $query = "PRAGMA journal_mode = WAL;"
        foreach ($tabela in $tabelas) {
            foreach ($conn in $this.connectionPool[$tabela]) {
                $command = $conn.CreateCommand()
                $command.CommandText = $query
                $command.ExecuteNonQuery()
            }
        }
    }
    [void] SetBusyTimeout($timeout) {
        $query = "PRAGMA busy_timeout = $timeout;"
        foreach ($tabela in $tabelas) {
            foreach ($conn in $this.connectionPool[$tabela]) {
                $command = $conn.CreateCommand()
                $command.CommandText = $query
                $command.ExecuteNonQuery()
            }
        }
    }
    [void] SetSynchronousNormal() {
        $query = "PRAGMA synchronous = NORMAL;"
        foreach ($tabela in $tabelas) {
            foreach ($conn in $this.connectionPool[$tabela]) {
                $command = $conn.CreateCommand()
                $command.CommandText = $query
                $command.ExecuteNonQuery()
            }
        }
    }
    [object] GetConnectionFromPool($tabela) {
        foreach ($conn in $this.connectionPool[$tabela]) {
            if ($conn.State -eq 'Open') {
                return $conn
            }
        }

        # Se não houver conexões disponíveis no pool, cria uma nova
        $connection = New-SQLiteConnection -DataSource $this.databasePath
        $this.connectionPool[$tabela].Add($connection)
        return $connection
    }

    [bool] isClient($id) {
        $sqlQuery = "SELECT id FROM clientes WHERE id = $id;"
        $selectedRecord = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool("clientes")
        if($selectedRecord){
            return $true
        }
        return $false
    }

    [object] getClient($id) {
        $sqlQuery = "SELECT nome, limite FROM clientes WHERE id = ${id};"
        $cliente = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool("clientes")
        return $cliente
    }

    [object] getSaldo($id) {
        $sqlQuery = "SELECT valor FROM saldos WHERE cliente_id = ${id};"
        $saldo = Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool("saldos")
        return $saldo
    }

    [void] updateSaldo($id, $saldo) {
        $sqlQuery = "UPDATE saldos SET valor = $saldo WHERE cliente_id = $id;"
        Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool("saldos")
    }

    [void] saveTransacao($id, $valor, $tipo, $descricao) {
        $sqlQuery = "INSERT INTO transacoes (cliente_id, valor, tipo, descricao, realizada_em) VALUES ($id, $valor, '$tipo', '$descricao', datetime('now'));"
        Invoke-SqliteQuery -Query $sqlQuery -SQLiteConnection $this.GetConnectionFromPool("transacoes")
    }

    [object] getTransacoes($id) {
        $sqlQuery = "SELECT valor, tipo, descricao, realizada_em FROM transacoes WHERE cliente_id
