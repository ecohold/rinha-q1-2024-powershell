#!/bin/bash

# Remove o banco de dados existente
rm -rf ./data/database.db

# Derruba os containers Docker existentes e remove os órfãos
docker-compose down --remove-orphans

# Levanta os containers Docker novamente, reconstruindo se necessário
docker-compose up --build
